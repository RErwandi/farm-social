﻿using System.Linq;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace Reynold
{
    public class FarmEditorWindow : OdinMenuEditorWindow
    {
        [MenuItem("Tools/Farm/Database Editor")]
        private static void Open()
        {
            var window = GetWindow<FarmEditorWindow>();
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 500);
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree(true);
            tree.DefaultMenuStyle.IconSize = 28.00f;
            tree.Config.DrawSearchToolbar = true;

            // Add all scriptable object items.
            tree.AddAllAssetsAtPath("", "Assets/_Database", typeof(BasePlantable), true);

            // Add icons to items.
            tree.EnumerateTree().AddIcons<BasePlantable>(x => x.itemIcon);

            return tree;
        }
        
        protected override void OnBeginDrawEditors()
        {
            var selected = this.MenuTree.Selection.FirstOrDefault();
            var toolbarHeight = this.MenuTree.Config.SearchToolbarHeight;

            // Draws a toolbar with the name of the currently selected menu item.
            SirenixEditorGUI.BeginHorizontalToolbar(toolbarHeight);
            {
                if (selected != null)
                {
                    GUILayout.Label(selected.Name);
                }

                if (SirenixEditorGUI.ToolbarButton(new GUIContent("Create Crop")))
                {
                    ScriptableObjectCreator.ShowDialog<Crop>("Assets/_Database/Crops", obj =>
                    {
                        obj.itemName = obj.name;
                        base.TrySelectMenuItemWithObject(obj); // Selects the newly created item in the editor
                    });
                }

                if (SirenixEditorGUI.ToolbarButton(new GUIContent("Create Tree")))
                {
                    ScriptableObjectCreator.ShowDialog<Tree>("Assets/_Database/Trees", obj =>
                    {
                        obj.itemName = obj.name;
                        base.TrySelectMenuItemWithObject(obj); // Selects the newly created item in the editor
                    });
                }
            }
            SirenixEditorGUI.EndHorizontalToolbar();
        }
    }
}