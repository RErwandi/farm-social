﻿using Reynold.Tools;

namespace Reynold
{
    public class CurrencyManager : PersistentSingleton<CurrencyManager>
    {
        public int gold = 0;
        public int diamond  = 0;
        public int trophy = 0;

        public bool HasEnoughGold(int amount)
        {
            return gold - amount >= 0;
        }
        
        public bool HasEnoughDiamond(int amount)
        {
            return diamond - amount >= 0;
        }
        
        public bool HasEnoughTrophy(int amount)
        {
            return trophy - amount >= 0;
        }
    }
}