﻿using Reynold.Tools;
using UnityEngine;

namespace Reynold
{
    public class ShopManager : PersistentSingleton<ShopManager>
    {
        public ShopButton shopButton;
        public Transform cropsContentView;
        public Crop[] crops;
        public Tree[] trees;

        protected override void Awake()
        {
            base.Awake();
            foreach (var crop in crops)
            {
                var btn = Instantiate(shopButton, cropsContentView);
                btn.SetButton(crop);
            }
        }
    }
}