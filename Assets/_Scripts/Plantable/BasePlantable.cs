﻿using UnityEngine;

namespace Reynold
{
    public class BasePlantable : ScriptableObject
    {
        public string itemId = "uniquekey";
        public string itemName = "new crop";
        public Sprite itemIcon = null;
        public GameObject itemPrefab = null;
        public int itemPrice = 0;
        public int itemHarvestDuration = 300;
    }
}