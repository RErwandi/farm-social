﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reynold
{
    [RequireComponent(typeof(PlayerController))]
    public class PlayerAnimation : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        private PlayerInput input;
        private float forwardSpeed;
        
        private static readonly int Speed = Animator.StringToHash("Speed");
        private const float GroundAcceleration = 10f;
        private const float GroundDeceleration = 5f;
        private bool IsMoveInput => !Mathf.Approximately(input.Movement.sqrMagnitude, 0f);

        private void Awake()
        {
            input = GetComponent<PlayerInput>();
            if(animator == null)
                animator = GetComponent<Animator>();
        }

        private void Update()
        {
            // Cache the move input and cap it's magnitude at 1.
            var moveInput = input.Movement;
            if (moveInput.sqrMagnitude > 1f)
                moveInput.Normalize();

            // Calculate the speed intended by input.
            var desiredForwardSpeed = moveInput.magnitude;

            // Determine change to speed based on whether there is currently any move input.
            var acceleration = IsMoveInput ? GroundAcceleration : GroundDeceleration;

            // Adjust the forward speed towards the desired speed.
            forwardSpeed = Mathf.MoveTowards(forwardSpeed, desiredForwardSpeed, acceleration * Time.deltaTime);

            // Set the animator parameter to control what animation is being played.
            animator.SetFloat(Speed, forwardSpeed);
            //animator.SetFloat("SpeedMultiplier", controller.Sprint ? 1.5f : 1f);
            //animator.SetBool("IsGrounded", controller.CharacterController.isGrounded);
        }
    }
}