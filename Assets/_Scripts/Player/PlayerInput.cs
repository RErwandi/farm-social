﻿using Reynold.Tools;
using UnityEngine;

namespace Reynold
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private KeyCode actionKey = KeyCode.E;
        [SerializeField] private KeyCode removeKey = KeyCode.X;
        
        private Vector2 movementVector;
        public Vector2 Movement => this.movementVector;

        private void Update()
        {
            this.movementVector.x = Input.GetAxisRaw("Horizontal");
            this.movementVector.y = Input.GetAxisRaw("Vertical");

            if (Input.GetKeyDown(actionKey))
                GameEvent.Trigger("Action");
            if(Input.GetKeyDown(removeKey))
                GameEvent.Trigger("Remove");
        }
    }
}