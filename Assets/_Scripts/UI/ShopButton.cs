﻿using UnityEngine;
using UnityEngine.UI;

namespace Reynold
{
    public class ShopButton : MonoBehaviour
    {
        [SerializeField] private Image icon = null;

        public void SetButton(BasePlantable item)
        {
            icon.sprite = item.itemIcon;
        }
    }
}